/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientecnologies.orient.server.network.protocol.http.api.v1.resource;

import com.jayway.restassured.response.Response;
import com.orientecnologies.orient.server.network.protocol.http.base.ServiceBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Enrico Risa on 23/06/15.
 */
@Test
public class OPropertyResourceTest extends ServiceBaseTest {

  Map<String, Object> params = new HashMap<String, Object>() {
                               {
                                 put("name", "test");
                                 put("type", "STRING");
                               }
                             };

  @Test(priority = 1)
  public void testCreateProperty() {

    Response post = basicAdmin().body(params).post("/api/v1/databases/baseTest/classes/V/properties");

    Assert.assertEquals(post.statusCode(), 201);

    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("name"), params.get("name"));
    Assert.assertEquals(as.get("type"), params.get("type"));

  }

  @Test(priority = 2)
  public void testGetProperty() {

    Response get = basicAdmin().get("/api/v1/databases/baseTest/classes/V/properties/test");
    Assert.assertEquals(get.statusCode(), 200);

    Map as = get.as(Map.class);

    Assert.assertEquals(as.get("name"), params.get("name"));
    Assert.assertEquals(as.get("type"), params.get("type"));
  }

  @Test(priority = 3)
  public void testAlterProperty() {

    params.put("name", "test1");
    Response post = basicAdmin().body(params).patch("/api/v1/databases/baseTest/classes/V/properties/test");

    Assert.assertEquals(post.statusCode(), 200);

    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("name"), params.get("name"));
    Assert.assertEquals(as.get("type"), params.get("type"));

    Response get = basicAdmin().get("/api/v1/databases/baseTest/classes/V/properties/test");
    Assert.assertEquals(get.statusCode(), 404);
  }

  @Test(priority = 4)
  public void testDropProperty() {
    Response post = basicAdmin().delete("/api/v1/databases/baseTest/classes/V/properties/test1");
    Assert.assertEquals(post.statusCode(), 204);

    Response get = basicAdmin().get("/api/v1/databases/baseTest/classes/V/properties/test1");
    Assert.assertEquals(get.statusCode(), 404);

  }

}
