/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientecnologies.orient.server.network.protocol.http.api.v1.resource;

import com.jayway.restassured.response.Response;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientecnologies.orient.server.network.protocol.http.base.ServiceBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Enrico Risa on 23/06/15.
 */
@Test
public class ODocumentResourceTest extends ServiceBaseTest {

  public static final String API_TEST = "/api/v1/databases/baseTest/documents/V";

  @Test(priority = 1)
  public void postDocument() {

    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", "Enrico");
        put("surname", "Risa");
        put("age", 32);
      }
    };

    Response post = basicAdmin().body(params).post(API_TEST);

    Assert.assertEquals(post.statusCode(), 201);
    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("name"), "Enrico");
    Assert.assertEquals(as.get("surname"), "Risa");
    Assert.assertEquals(as.get("age"), 32);
    Assert.assertEquals(as.get("@class"), "V");
  }

  @Test(priority = 2)
  public void putDocument() {

    // CREATE FIRST
    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", "Mario");
        put("surname", "Rossi");
        put("age", 34);
      }
    };

    Response post = basicAdmin().body(params).post(API_TEST);

    Assert.assertEquals(post.statusCode(), 201);
    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("name"), "Mario");
    Assert.assertEquals(as.get("surname"), "Rossi");
    Assert.assertEquals(as.get("age"), 34);
    Assert.assertEquals(as.get("@class"), "V");
    Assert.assertEquals(as.get("@version"), 1);
    Assert.assertNotNull(as.get("@rid"));

    ORecordId recordId = new ORecordId((String) as.get("@rid"));
    Assert.assertEquals(recordId.isValid(), true);
    Assert.assertEquals(recordId.isTemporary(), false);
    Assert.assertEquals(recordId.isNew(), false);
    Assert.assertEquals(recordId.isPersistent(), true);

    as.remove("age");
    Response put = basicAdmin().body(as).put(API_TEST + "/" + as.get("@rid"));

    Assert.assertEquals(put.statusCode(), 200);
    as = put.as(Map.class);
    Assert.assertEquals(as.get("name"), "Mario");
    Assert.assertEquals(as.get("surname"), "Rossi");
    Assert.assertEquals(as.get("age"), null);
    Assert.assertEquals(as.get("@version"), 2);
    Assert.assertEquals(as.get("@class"), "V");

  }

  @Test(priority = 3)
  public void patchDocument() {

    // CREATE FIRST
    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", "Matt");
        put("surname", "Dillon");
        put("age", 45);
      }
    };

    Response post = basicAdmin().body(params).post(API_TEST);

    Assert.assertEquals(post.statusCode(), 201);
    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("name"), "Matt");
    Assert.assertEquals(as.get("surname"), "Dillon");
    Assert.assertEquals(as.get("age"), 45);
    Assert.assertEquals(as.get("@class"), "V");
    Assert.assertEquals(as.get("@version"), 1);
    Assert.assertNotNull(as.get("@rid"));

    ORecordId recordId = new ORecordId((String) as.get("@rid"));
    Assert.assertEquals(recordId.isValid(), true);
    Assert.assertEquals(recordId.isTemporary(), false);
    Assert.assertEquals(recordId.isNew(), false);
    Assert.assertEquals(recordId.isPersistent(), true);

    as.remove("age");
    as.remove("surname");
    as.put("limit", 2);
    as.put("name", "Kevin");
    Response put = basicAdmin().body(as).patch(API_TEST + "/" + as.get("@rid"));

    Assert.assertEquals(put.statusCode(), 200);
    as = put.as(Map.class);
    Assert.assertEquals(as.get("name"), "Kevin");
    Assert.assertEquals(as.get("surname"), "Dillon");
    Assert.assertEquals(as.get("age"), 45);
    Assert.assertEquals(as.get("limit"), 2);
    Assert.assertEquals(as.get("@version"), 2);
    Assert.assertEquals(as.get("@class"), "V");

  }

  @Test(priority = 4)
  public void deleteDocument() {

  }
}
