/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientecnologies.orient.server.network.protocol.http.api.v1.resource;

import com.jayway.restassured.response.Response;
import com.orientecnologies.orient.server.network.protocol.http.base.ServiceBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.get;

/**
 * Created by Enrico Risa on 20/06/15.
 */
@Test
public class ODatabaseResourceTest extends ServiceBaseTest {

  @Test(priority = 1)
  public void testCreateDatabase() {

    Map<String, String> params = new HashMap<String, String>() {
      {
        put("name", "databaseResource");
        put("storage", "memory");
        put("type", "graph");
      }
    };

    Response post = basicRoot().body(params).post("/api/v1/databases");

    Assert.assertEquals(post.statusCode(), 201);
    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("name"), "databaseResource");
  }

  @Test(priority = 1)
  public void testCreateDatabaseException() {

    Map<String, String> params = new HashMap<String, String>() {
      {
        put("type", "graph");
      }
    };

    Response post = basicRoot().body(params).post("/api/v1/databases");

    Assert.assertEquals(post.statusCode(), 400);
    Map as = post.as(Map.class);

    Assert.assertEquals(as.get("status"), 400);
  }

  @Test(priority = 2)
  public void listDatabases() {

    checkDatabase(true);

  }

  private void checkDatabase(boolean present) {
    Response response = get("/api/v1/databases");
    Map as = response.as(Map.class);

    Assert.assertEquals(response.statusCode(), 200);

    Assert.assertEquals(as.keySet().contains("databaseResource"), present);
  }

  @Test(priority = 3)
  public void dropDatabase() {

    Response delete = basicRoot().delete("/api/v1/databases/databaseResource");

    Assert.assertEquals(delete.statusCode(), 200);

    checkDatabase(false);
  }
}
