/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientecnologies.orient.server.network.protocol.http.api.v1.resource;

import com.jayway.restassured.response.Response;
import com.orientecnologies.orient.server.network.protocol.http.base.ServiceBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Enrico Risa on 27/06/15.
 */
public class OClassIndexesResourceTest extends ServiceBaseTest {

  @Test(priority = 1)
  public void createIndexTest() {
    createQuickClass("TestIndex");
    createStringProperty("TestIndex", "name");

    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", "TestIndex.name");
        put("type", "NOTUNIQUE");
        put("fields", new String[] { "name" });
      }
    };

    String url = String.format("/api/v1/databases/baseTest/classes/%s/indexes", "TestIndex");
    Response response = basicAdmin().body(params).post(url);
    Assert.assertEquals(response.statusCode(), 201);

    Map as = response.as(Map.class);

    Assert.assertEquals(as.get("name"), "TestIndex.name");
    Assert.assertEquals(as.get("type"), "NOTUNIQUE");

    Map test = getClass("TestIndex");

    Collection indexes = (Collection) test.get("indexes");
    Assert.assertEquals(indexes.size(), 1);

  }

  @Test(priority = 2)
  public void getClassIndexesTest() {

    String url = String.format("/api/v1/databases/baseTest/classes/%s/indexes", "TestIndex");
    Response response = basicAdmin().get(url);
    Assert.assertEquals(response.statusCode(), 200);

    List list = response.as(List.class);

    Assert.assertEquals(list.size(), 1);
  }

  @Test(priority = 3)
  public void rebuildIndexTest() {

    String url = String.format("/api/v1/databases/baseTest/classes/%s/indexes/%s", "TestIndex", "TestIndex.name");
    Response response = basicAdmin().put(url);
    Assert.assertEquals(response.statusCode(), 204);
  }

  @Test(priority = 4)
  public void dropIndexTest() {

    String url = String.format("/api/v1/databases/baseTest/classes/%s/indexes/%s", "TestIndex", "TestIndex.name");
    Response response = basicAdmin().delete(url);
    Assert.assertEquals(response.statusCode(), 204);

    Map test = getClass("TestIndex");

    Collection indexes = (Collection) test.get("indexes");
    Assert.assertEquals(indexes.size(), 0);
  }
}
