package com.orientecnologies.orient.server.network.protocol.http.api.v1.resource;

import com.jayway.restassured.response.Response;
import com.orientecnologies.orient.server.network.protocol.http.base.ServiceBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Enrico Risa on 21/06/15.
 */
@Test
public class OClassResourceTest extends ServiceBaseTest {

  @Test(priority = 1)
  public void createSingleClass() {

    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", "Test");
        put("abstract", true);
      }
    };
    Response aClass = createClass(params);

    Map cls = aClass.as(Map.class);
    Assert.assertEquals(cls.get("name"), "Test");
    Assert.assertEquals(cls.get("abstract"), true);
  }

  @Test(priority = 2)
  public void getSingleClassTest() {

    Map cls = getClass("Test");

    Assert.assertEquals(cls.get("name"), "Test");
    Assert.assertEquals(cls.get("abstract"), true);
  }

  @Test(priority = 3)
  public void patchSingleClass() {

    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", "Test1");
      }
    };
    Response patch = basicAdmin().body(params).patch(classUrl("Test"));

    Assert.assertEquals(patch.statusCode(), 200);

    Map cls = patch.as(Map.class);

    Assert.assertEquals(cls.get("name"), "Test1");
    Assert.assertEquals(cls.get("abstract"), true);

    Response test = getClassResponse("Test");
    Assert.assertEquals(test.statusCode(), 404);
  }
}
