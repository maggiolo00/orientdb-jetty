package com.orientecnologies.orient.server.network.protocol.http.base;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.orientechnologies.orient.core.Orient;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.OServerMain;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class ServiceBaseTest {

  OServer oServer;

  @BeforeTest
  public void serverSetup() {
    try {
      oServer = OServerMain.create();
      oServer.startup(ServiceBaseTest.class.getClassLoader().getResourceAsStream("orientdb-server-config.xml"));
      oServer.activate();

      ODatabaseDocumentTx database = Orient.instance().getDatabaseFactory().createDatabase("graph", "memory:baseTest");
      database.create();

      RestAssured.port = 8080;
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @AfterTest
  public void serverShutdown() {
    oServer.shutdown();
  }

  public static RequestSpecification basicRoot() {
    return given().auth().preemptive().basic("root", "root");
  }

  public RequestSpecification basicAdmin() {
    return given().auth().preemptive().basic("admin", "admin");
  }

  protected Response createQuickClass(final String clazz) {
    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", clazz);
      }
    };
    return createClass(params);
  }

  protected Response createClass(Map<String, Object> params) {

    String url = String.format("/api/v1/databases/baseTest/classes");
    Response response = basicAdmin().body(params).post(url);
    Assert.assertEquals(response.statusCode(), 201);

    return response;
  }

  protected Map getClass(final String clazz) {

    Response response = getClassResponse(clazz);
    Assert.assertEquals(response.statusCode(), 200);
    return response.as(Map.class);
  }

  protected Response getClassResponse(final String clazz) {

    String url = String.format("/api/v1/databases/baseTest/classes/%s", clazz);
    Response response = basicAdmin().get(url);
    return response;
  }

  protected Response createStringProperty(final String clazz, final String propertyName) {

    return createProperty(clazz, propertyName, "string");
  }

  protected Response createProperty(final String clazz, final String propertyName, final String type) {

    Map<String, Object> params = new HashMap<String, Object>() {
      {
        put("name", propertyName);
        put("type", type);
      }
    };
    String url = String.format("/api/v1/databases/baseTest/classes/%s/properties", clazz);
    Response response = basicAdmin().body(params).post(url);
    Assert.assertEquals(response.statusCode(), 201);

    return response;
  }

  public String classUrl(String clazz) {
    String url = String.format("/api/v1/databases/baseTest/classes/%s", clazz);
    return url;
  }
}
