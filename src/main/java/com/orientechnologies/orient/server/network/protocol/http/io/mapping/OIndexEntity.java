/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.io.mapping;

import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;

/**
 * Created by Enrico Risa on 27/06/15.
 */
public class OIndexEntity extends OEntity<OIndex> {

  public OIndexEntity(OIndex idx, OAbstractIO io) {
    super(idx, io);
  }

  @Override
  public void serialize(OIOWriter writer) throws Exception {

    writer.startObject();
    writer.writeAttribute("name", entity.getName());
    writer.writeAttribute("type", entity.getType());

    final OIndexDefinition indexDefinition = entity.getDefinition();
    if (indexDefinition != null && !indexDefinition.getFields().isEmpty())
      writer.writeAttribute("fields", indexDefinition.getFields());
    writer.endObject();

  }
}
