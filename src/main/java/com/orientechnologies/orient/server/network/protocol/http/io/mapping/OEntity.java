package com.orientechnologies.orient.server.network.protocol.http.io.mapping;

import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public abstract class OEntity<T> {

  protected final T       entity;
  protected OAbstractIO io;

  public OEntity(T entity, OAbstractIO io) {
    this.entity = entity;
    this.io = io;
  }

  public String serialize() throws Exception {
    OIOWriter writer = io.writer();
    serialize(writer);
    return writer.end();
  }

  public abstract void serialize(OIOWriter writer) throws Exception;

  public OAbstractIO io() {
    return io;
  }
}
