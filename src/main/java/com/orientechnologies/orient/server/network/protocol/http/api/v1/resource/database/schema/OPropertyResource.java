/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database.schema;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.OPropertyService;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.OResponseBuilder;
import com.orientechnologies.orient.server.network.protocol.http.io.mapping.OPropertyEntity;
import com.orientechnologies.orient.server.network.protocol.http.providers.DB;
import com.orientechnologies.orient.server.network.protocol.http.providers.IO;
import com.orientechnologies.orient.server.network.protocol.http.providers.annotations.PATCH;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by Enrico Risa on 23/06/15.
 */
@Singleton
public class OPropertyResource {

  @Inject
  OPropertyService propertyService;

  @Inject
  OResponseBuilder builder;

  @POST
  public Response create(String body, @PathParam("className") String name, @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {
      Map<String, Object> property = io.reader().parseMap(body);
      OProperty oProperty = propertyService.createProperty(db, name, property);
      return builder.created(new OPropertyEntity(oProperty, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }

  @PATCH
  @Path("{property}")
  public Response patch(String body, @PathParam("className") String name, @PathParam("property") String property,
      @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {
      Map<String, Object> prop = io.reader().parseMap(body);
      OProperty oProperty = propertyService.patchProperty(db, name, property, prop);
      return builder.ok(new OPropertyEntity(oProperty, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }

  @DELETE
  @Path("{property}")
  public Response drop(String body, @PathParam("className") String name, @PathParam("property") String property,
      @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {
    propertyService.dropProperty(db, name, property);
    return builder.noContent();
  }

  @GET
  @Path("{property}")
  public Response get(@PathParam("className") String name, @PathParam("property") String property, @DB ODatabaseDocumentTx db,
      @IO OAbstractIO io) {
    OProperty prop = propertyService.getProperty(db, name, property);
    return builder.ok(new OPropertyEntity(prop, io));
  }

}
