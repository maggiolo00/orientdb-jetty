package com.orientechnologies.orient.server.network.protocol.http.providers;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.network.protocol.http.utils.Utils;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.internal.inject.AbstractContainerRequestValueFactory;
import org.glassfish.jersey.server.internal.inject.AbstractValueFactoryProvider;
import org.glassfish.jersey.server.internal.inject.MultivaluedParameterExtractorProvider;
import org.glassfish.jersey.server.internal.inject.ParamInjectionResolver;
import org.glassfish.jersey.server.model.Parameter;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Singleton
public class ODatabaseFactoryProvider extends AbstractValueFactoryProvider {

  @Inject
  UriInfo info;

  @Inject
  OServer server;

  @Inject
  public ODatabaseFactoryProvider(MultivaluedParameterExtractorProvider mpep, ServiceLocator locator) {
    super(mpep, locator, Parameter.Source.UNKNOWN);
  }

  @Override
  protected Factory<?> createValueFactory(Parameter parameter) {

    Class<?> classType = parameter.getRawType();
    if (classType != null && classType.equals(ODatabaseDocumentTx.class)) {
      return new ODatabaseFactory();
    }
    return null;
  }

  @Singleton
  public static class ODatabaseInjectResolver extends ParamInjectionResolver<DB> {

    public ODatabaseInjectResolver() {
      super(ODatabaseFactoryProvider.class);
    }
  }

  protected class ODatabaseFactory extends AbstractContainerRequestValueFactory<ODatabaseDocumentTx> {

    @Override
    public ODatabaseDocumentTx provide() {

      MultivaluedMap<String, String> pathParameters = info.getPathParameters();
      String database = pathParameters.getFirst("database");
      ContainerRequest containerRequest = getContainerRequest();
      MultivaluedMap<String, String> headers = containerRequest.getHeaders();
      String[] auth = Utils.decodeCredential(headers.getFirst("Authorization"));

      if (auth == null) {
        throw new WebApplicationException(Response.Status.UNAUTHORIZED);
      }

      ODatabaseDocumentTx document = (ODatabaseDocumentTx) server.openDatabase("document", database, auth[0], auth[1]);
      return document;
    }
  }

}
