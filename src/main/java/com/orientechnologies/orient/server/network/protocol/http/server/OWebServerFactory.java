package com.orientechnologies.orient.server.network.protocol.http.server;

/**
 * Created by Enrico Risa on 17/06/15.
 */
public class OWebServerFactory {

  public static OWebServerFactory INSTANCE = new OWebServerFactory();

  public OWebServer createWebServer() {
    return new OWebServerJetty();
  }
}
