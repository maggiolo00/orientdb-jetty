/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.io.mapping;

import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;

import java.lang.reflect.Constructor;
import java.util.Collection;

/**
 * Created by Enrico Risa on 28/06/15.
 */
public class OCollectionEntity<T, E extends OEntity> extends OEntity<Collection<T>> {

  private final Class<E> handler;

  public OCollectionEntity(Collection<T> entities, Class<E> handler, OAbstractIO io) {
    super(entities, io);
    this.handler = handler;
  }

  @Override
  public void serialize(OIOWriter writer) throws Exception {

    writer.startCollection();

    Constructor<E> constructor = null;
    for (T t : entity) {
      if (constructor == null)
        constructor = findConstructor(t);
      E e = constructor.newInstance(t, io);
      e.serialize(writer);
    }

    writer.endCollection();
  }

  private Constructor<E> findConstructor(T t) {
    Constructor<?>[] declaredConstructors = handler.getDeclaredConstructors();
    for (Constructor<?> declaredConstructor : declaredConstructors) {
      Class<?>[] parameterTypes = declaredConstructor.getParameterTypes();
      if (parameterTypes[0].isAssignableFrom(t.getClass())) {
        return (Constructor<E>) declaredConstructor;
      }
    }
    return null;
  }
}
