/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.io.mapping;

import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.core.metadata.schema.OPropertyImpl;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;

import java.util.Map;

/**
 * Created by Enrico Risa on 23/06/15.
 */
public class OPropertyEntity extends OEntity<OProperty> {

  public OPropertyEntity(OProperty property, OAbstractIO io) {
    super(property, io);
  }

  @Override
  public void serialize(OIOWriter writer) throws Exception {

    writer.startObject();
    if (entity != null) {
      writer.writeAttribute("name", entity.getName());
      if (entity.getLinkedClass() != null)
        writer.writeAttribute("linkedClass", entity.getLinkedClass().getName());
      if (entity.getLinkedType() != null)
        writer.writeAttribute("linkedType", entity.getLinkedType().toString());
      writer.writeAttribute("type", entity.getType().toString());
      writer.writeAttribute("mandatory", entity.isMandatory());
      writer.writeAttribute("readonly", entity.isReadonly());
      writer.writeAttribute("notNull", entity.isNotNull());
      writer.writeAttribute("min", entity.getMin());
      writer.writeAttribute("max", entity.getMax());
      writer.writeAttribute("regexp", entity.getRegexp());
      writer.writeAttribute("collate", entity.getCollate() != null ? entity.getCollate().getName() : "default");

      if (entity instanceof OPropertyImpl) {
        final Map<String, String> custom = ((OPropertyImpl) entity).getCustomInternal();
        if (custom != null && !custom.isEmpty()) {
          writer.writeAttribute("custom", custom);
        }
      }
    }
    writer.endObject();
  }
}
