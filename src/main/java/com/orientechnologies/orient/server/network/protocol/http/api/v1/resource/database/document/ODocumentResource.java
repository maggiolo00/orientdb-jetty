/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database.document;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.ORecordNotFoundException;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.ORecordInternal;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.OResponseBuilder;
import com.orientechnologies.orient.server.network.protocol.http.io.mapping.ODocumentEntity;
import com.orientechnologies.orient.server.network.protocol.http.providers.DB;
import com.orientechnologies.orient.server.network.protocol.http.providers.IO;
import com.orientechnologies.orient.server.network.protocol.http.providers.annotations.PATCH;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public class ODocumentResource {

  @Inject
  OResponseBuilder builder;

  @POST
  @Path("{className}")
  public Response createDocument(@PathParam("className") String className, String body, @DB ODatabaseDocumentTx db,
      @IO OAbstractIO io) {

    try {
      ODocument document = io.reader().parseDocument(body);
      // ASSURE TO MAKE THE RECORD ID INVALID
      ((ORecordId) document.getIdentity()).clusterPosition = ORID.CLUSTER_POS_INVALID;
      document.setClassName(className);
      ODocument saved = db.save(document);
      return builder.created(new ODocumentEntity(saved, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }

  // TODO
  @PUT
  @Path("{className}/{rid}")
  public Response putDocument(@PathParam("className") String className, @PathParam("rid") String rid, String body,
      @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {

      ORecordId recordId = new ORecordId(rid);
      final ODocument currentDocument = db.load(recordId);
      if (currentDocument == null) {
        String msg = String.format("Record %s not found.", rid);
        throw new ORecordNotFoundException(msg);
      }
      ODocument newDocument = io.reader().parseDocument(body);
      ORecordInternal.setIdentity(newDocument, recordId);
      currentDocument.merge(newDocument, false, false);
      currentDocument.save();
      return builder.ok(new ODocumentEntity(currentDocument, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }

  }

  // TODO
  @PATCH
  @Path("{className}/{rid}")
  public Response patchDocument(@PathParam("className") String className, @PathParam("rid") String rid, String body,
      @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {

      ORecordId recordId = new ORecordId(rid);
      final ODocument currentDocument = db.load(recordId);
      if (currentDocument == null) {
        String msg = String.format("Record %s not found.", rid);
        throw new ORecordNotFoundException(msg);
      }
      ODocument newDocument = io.reader().parseDocument(body);
      ORecordInternal.setIdentity(newDocument, recordId);
      currentDocument.merge(newDocument, true, false);
      currentDocument.save();
      return builder.ok(new ODocumentEntity(currentDocument, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }

  }

  @DELETE
  @Path("{className}/{rid}")
  public Response deleteDocument(@PathParam("className") String className, @PathParam("rid") String rid,
      @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {
      ORecord load = db.load(new ORecordId(rid));
      db.delete(load);
      return builder.ok();
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }
}
