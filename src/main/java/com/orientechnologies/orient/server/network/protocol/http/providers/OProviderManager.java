package com.orientechnologies.orient.server.network.protocol.http.providers;

/**
 * Created by Enrico Risa on 18/06/15.
 */

public class OProviderManager {

  public String packages() {
    return ODatabaseFactoryProvider.class.getPackage().getName();
  }
}
