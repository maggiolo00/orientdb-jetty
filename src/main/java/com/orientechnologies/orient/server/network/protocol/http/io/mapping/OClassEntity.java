/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.io.mapping;

import com.orientechnologies.orient.core.exception.OSecurityAccessException;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;

import java.util.List;
import java.util.Set;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public class OClassEntity extends OEntity<OClass> {

  public OClassEntity(OClass clazz, OAbstractIO io) {
    super(clazz, io);
  }

  @Override
  public void serialize(OIOWriter writer) throws Exception {

    writer.startObject();

    writeHeader(writer);

    writeProperties(writer);

    writeIndexes(writer);

    writer.endObject();
  }

  private void writeIndexes(OIOWriter writer) throws Exception {
    final Set<OIndex<?>> indexes = entity.getIndexes();

    writer.startCollection("indexes");
    if (!indexes.isEmpty()) {
      for (final OIndex<?> index : indexes) {
        OIndexEntity indexEntity = new OIndexEntity(index, io);
        indexEntity.serialize(writer);
      }
    }
    writer.endCollection();
  }

  private void writeProperties(OIOWriter writer) throws Exception {
    writer.startCollection("properties");
    if (entity.properties() != null && entity.properties().size() > 0) {

      for (final OProperty prop : entity.properties()) {
        OPropertyEntity oPropertyEntity = new OPropertyEntity(prop, io);
        oPropertyEntity.serialize(writer);
      }
    }
    writer.endCollection();
  }

  private void writeHeader(OIOWriter writer) throws Exception {
    writer.writeAttribute("name", entity.getName());
    writer.startCollection("superClasses");
    List<OClass> superClasses = entity.getSuperClasses();
    for (OClass superClass : superClasses) {
      writer.writeValue(superClass.getName());
    }
    writer.endCollection();
    writer.writeAttribute("alias", entity.getShortName());
    writer.writeAttribute("abstract", entity.isAbstract());
    writer.writeAttribute("strictmode", entity.isStrictMode());
    writer.writeAttribute("defaultCluster", entity.getDefaultClusterId());
    writer.writeAttribute("clusterSelection", entity.getClusterSelection().getName());
    try {
      writer.writeAttribute("records", entity.getSize());
    } catch (OSecurityAccessException e) {
      writer.writeAttribute("records", "? (Unauthorized)");
    }
  }
}
