/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.api.v1.services;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.core.metadata.schema.OType;

import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by Enrico Risa on 23/06/15.
 */
@Singleton
public class OPropertyService {

  public OProperty createProperty(ODatabaseDocumentTx db, String className, Map<String, Object> properties) {

    checkPropertyInput(properties);
    OClass aClass = getClass(db, className);

    String name = (String) properties.get("name");
    String type = (String) properties.get("type");

    OProperty property = aClass.createProperty(name, OType.valueOf(type.toUpperCase()));
    return property;
  }

  private OClass getClass(ODatabaseDocumentTx db, String className) {
    OClass aClass = db.getMetadata().getSchema().getClass(className);

    if (aClass == null) {
      classNotFound(className);
    }
    return aClass;
  }

  private void checkPropertyInput(Map<String, Object> properties) {
    if (properties.get("name") == null || properties.get("type") == null) {
      String msg = String.format("Name or type required.");
      throw new WebApplicationException(msg, Response.Status.BAD_REQUEST);
    }
  }

  public OProperty patchProperty(ODatabaseDocumentTx db, String className, String propertyName, Map<String, Object> properties) {

    OProperty internalProperty = getInternalProperty(db, className, propertyName);

    String name = (String) properties.get("name");
    String type = (String) properties.get("type");

    if (name != null) {
      internalProperty.setName(name);
    }
    if (type != null) {
      internalProperty.setType(OType.valueOf(type.toUpperCase()));
    }
    return internalProperty;
  }

  public void dropProperty(ODatabaseDocumentTx db, String className, String propertyName) {

    OClass oClass = getClass(db, className);

    oClass.dropProperty(propertyName);

  }

  public OProperty getProperty(ODatabaseDocumentTx db, String className, String propertyName) {

    return getInternalProperty(db, className, propertyName);
  }

  private OProperty getInternalProperty(ODatabaseDocumentTx db, String className, String propertyName) {
    OClass aClass = getClass(db, className);
    OProperty property = aClass.getProperty(propertyName);
    if (property == null) {
      propertyNotFound(className, propertyName);
    }
    return property;
  }

  protected void classNotFound(String name) {
    String msg = String.format("Class %s not found.", name);
    throw new WebApplicationException(msg, Response.Status.NOT_FOUND);
  }

  protected void propertyNotFound(String className, String name) {
    String msg = String.format("Property %s in class %s not found.", name, className);
    throw new WebApplicationException(msg, Response.Status.NOT_FOUND);
  }

}
