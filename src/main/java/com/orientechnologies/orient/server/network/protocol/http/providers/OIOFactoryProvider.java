/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.providers;

import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.OJsonIO;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.internal.inject.AbstractContainerRequestValueFactory;
import org.glassfish.jersey.server.internal.inject.AbstractValueFactoryProvider;
import org.glassfish.jersey.server.internal.inject.MultivaluedParameterExtractorProvider;
import org.glassfish.jersey.server.internal.inject.ParamInjectionResolver;
import org.glassfish.jersey.server.model.Parameter;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.ext.Provider;

/**
 * Created by Enrico Risa on 21/06/15.
 */
@Provider
@Singleton
public class OIOFactoryProvider extends AbstractValueFactoryProvider {

  @Inject
  public OIOFactoryProvider(MultivaluedParameterExtractorProvider mpep, ServiceLocator locator) {
    super(mpep, locator, Parameter.Source.UNKNOWN);
  }

  @Override
  protected Factory<?> createValueFactory(Parameter parameter) {

    Class<?> classType = parameter.getRawType();
    if (classType != null && classType.equals(OAbstractIO.class)) {
      return new OIOFactory();
    }
    return null;
  }

  @Singleton
  public static class OIOInjectResolver extends ParamInjectionResolver<IO> {
    public OIOInjectResolver() {
      super(OIOFactoryProvider.class);
    }
  }

  protected class OIOFactory extends AbstractContainerRequestValueFactory<OAbstractIO> {

    @Override
    public OAbstractIO provide() {
      return new OJsonIO();
    }
  }
}
