/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.binders;

import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.OClassService;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.ODatabaseService;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.OPropertyService;
import com.orientechnologies.orient.server.network.protocol.http.io.OResponseBuilder;
import com.orientechnologies.orient.server.network.protocol.http.providers.DB;
import com.orientechnologies.orient.server.network.protocol.http.providers.IO;
import com.orientechnologies.orient.server.network.protocol.http.providers.ODatabaseFactoryProvider;
import com.orientechnologies.orient.server.network.protocol.http.providers.OIOFactoryProvider;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.spi.internal.ValueFactoryProvider;

import javax.inject.Singleton;

/**
 * Created by Enrico Risa on 21/06/15.
 */
public class DefaultBinder extends AbstractBinder {

  private OServer server;

  public DefaultBinder(OServer server) {

    this.server = server;
  }

  @Override
  protected void configure() {
    bind(ODatabaseService.class).to(ODatabaseService.class).in(Singleton.class);
    bind(OClassService.class).to(OClassService.class).in(Singleton.class);
    bind(OPropertyService.class).to(OPropertyService.class).in(Singleton.class);
    bind(server).to(OServer.class);
    bind(OResponseBuilder.class).to(OResponseBuilder.class).in(Singleton.class);
    bind(ODatabaseFactoryProvider.class).to(ValueFactoryProvider.class).in(Singleton.class);
    bind(ODatabaseFactoryProvider.ODatabaseInjectResolver.class).to(new TypeLiteral<InjectionResolver<DB>>() {
    }).in(Singleton.class);
    bind(OIOFactoryProvider.OIOInjectResolver.class).to(new TypeLiteral<InjectionResolver<IO>>() {
    }).in(Singleton.class);
  }
}
