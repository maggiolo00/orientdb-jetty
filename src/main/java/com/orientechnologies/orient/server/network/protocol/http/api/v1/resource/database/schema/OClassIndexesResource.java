/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database.schema;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.OClassService;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.OResponseBuilder;
import com.orientechnologies.orient.server.network.protocol.http.io.mapping.OCollectionEntity;
import com.orientechnologies.orient.server.network.protocol.http.io.mapping.OIndexEntity;
import com.orientechnologies.orient.server.network.protocol.http.providers.DB;
import com.orientechnologies.orient.server.network.protocol.http.providers.IO;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Set;

/**
 * Created by Enrico Risa on 27/06/15.
 */

@Singleton
public class OClassIndexesResource {

  @Inject
  OResponseBuilder builder;

  @Inject
  OClassService    classService;

  @POST
  public Response postIndex(String body, @PathParam("className") String className, @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {
      Map<String, Object> params = io.reader().parseMap(body);
      OIndex index = classService.createIndex(db, className, params);
      return builder.created(new OIndexEntity(index, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }

  @GET
  public Response getIndexes(@PathParam("className") String className, @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    Set<OIndex<?>> indexes = classService.indexes(db, className);
    return builder.ok(new OCollectionEntity(indexes, OIndexEntity.class, io));
  }

  @PUT
  @Path("{indexName}")
  public Response rebuildIndex(@PathParam("className") String className, @PathParam("indexName") String indexName,
      @DB ODatabaseDocumentTx db) {

    classService.rebuildIndex(db, className, indexName);
    return builder.noContent();
  }

  @DELETE
  @Path("{indexName}")
  public Response dropIndex(@PathParam("className") String className, @PathParam("indexName") String indexName,
      @DB ODatabaseDocumentTx db) {
    classService.dropIndex(db, className, indexName);
    return builder.noContent();
  }
}
