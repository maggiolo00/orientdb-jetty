/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.io;

import com.fasterxml.jackson.core.JsonFactory;
import com.orientechnologies.orient.server.network.protocol.http.io.reader.OIOReader;
import com.orientechnologies.orient.server.network.protocol.http.io.reader.OIOReaderJson;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriterJson;

import javax.ws.rs.core.MediaType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Enrico Risa on 21/06/15.
 */
public class OJsonIO extends OAbstractIO {

  JsonFactory factory = new JsonFactory();



  public OJsonIO() {
    super(MediaType.APPLICATION_JSON_TYPE);
  }



  @Override
  protected OIOReader newReader() {
    return new OIOReaderJson();
  }

  @Override
  protected OIOWriter newWriter() throws IOException {
    return new OIOWriterJson(factory, new ByteArrayOutputStream());
  }
}
