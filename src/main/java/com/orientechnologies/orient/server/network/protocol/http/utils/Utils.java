/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.utils;

import sun.misc.BASE64Decoder;

/**
 * Created by Enrico Risa on 19/06/15.
 */

public class Utils {
  public static String[] decodeCredential(String authHeader) {
    try {
      if (authHeader != null && authHeader.startsWith("Basic")) {
        String base64Credentials = authHeader.substring("Basic".length()).trim();

        byte[] bytes = new BASE64Decoder().decodeBuffer(base64Credentials);
        String credentials = new String(bytes, "UTF-8");
        final String[] values = credentials.split(":", 2);

        return values;

      }
    } catch (Exception e) {
      e.printStackTrace();

    }
    return null;
  }
}
