/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.io;

import com.orientechnologies.orient.server.network.protocol.http.io.mapping.OEntity;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public class OResponseBuilder {

  public Response ok() {
    return ok(null);
  }

  public Response created(OEntity entity) {
    return toOutput(Response.created(null), entity).build();
  }

  public Response noContent() {
    return toOutput(Response.noContent(), null).build();
  }

  public Response ok(OEntity entity) {

    return toOutput(Response.ok(), entity).build();

  }

  public Response.ResponseBuilder toOutput(Response.ResponseBuilder ok, OEntity entity) {

    try {
      if (entity == null)
        return ok;
      return ok.entity(entity.serialize()).type(entity.io().media());
    } catch (Exception e) {
      e.printStackTrace();
      throw new WebApplicationException(e);
    }

  }

}
