package com.orientechnologies.orient.server.network.protocol.http.io;

import com.orientechnologies.orient.server.network.protocol.http.io.reader.OIOReader;
import com.orientechnologies.orient.server.network.protocol.http.io.writer.OIOWriter;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public abstract class OAbstractIO {

  protected MediaType mediaType;

  public OAbstractIO(MediaType mediaType) {
    this.mediaType = mediaType;
  }

  public OIOReader reader() {
    return newReader();
  }

  protected abstract OIOReader newReader();

  public OIOWriter writer() throws IOException {
    return newWriter();
  }

  protected abstract OIOWriter newWriter() throws IOException;

  public MediaType media() {
    return mediaType;
  }
}
