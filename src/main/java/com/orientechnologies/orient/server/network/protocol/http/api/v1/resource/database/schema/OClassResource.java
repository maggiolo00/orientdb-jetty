/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database.schema;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.OClassService;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.OResponseBuilder;
import com.orientechnologies.orient.server.network.protocol.http.io.mapping.OClassEntity;
import com.orientechnologies.orient.server.network.protocol.http.providers.DB;
import com.orientechnologies.orient.server.network.protocol.http.providers.IO;
import com.orientechnologies.orient.server.network.protocol.http.providers.annotations.PATCH;
import groovy.lang.Singleton;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by Enrico Risa on 20/06/15.
 */

@Singleton
public class OClassResource {

  @Inject
  OResponseBuilder builder;

  @Inject
  OClassService    classService;

  @GET
  @Path("{className}")
  public Response singleClass(@PathParam("className") String name, @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    OClass oClass = classService.singleClass(db, name);
    return builder.ok(new OClassEntity(oClass, io));
  }

  @DELETE
  @Path("{className}")
  public Response deleteClass(@PathParam("className") String name, @DB ODatabaseDocumentTx db) {
    classService.dropClass(db, name);
    return builder.noContent();
  }

  @Path("{className}/properties")
  public Class<OPropertyResource> properties() {
    return OPropertyResource.class;
  }

  @Path("{className}/indexes")
  public Class<OClassIndexesResource> indexes() {
    return OClassIndexesResource.class;
  }

  @PATCH
  @Path("{className}")
  public Response patchClass(String body, @PathParam("className") String name, @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {
      Map<String, Object> clazz = io.reader().parseMap(body);
      OClass oClass = classService.alterClass(db, name, clazz);
      return builder.ok(new OClassEntity(oClass, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }

  @POST
  public Response postClass(String body, @DB ODatabaseDocumentTx db, @IO OAbstractIO io) {

    try {
      Map<String, Object> clazz = io.reader().parseMap(body);
      OClass oClass = classService.createClass(db, clazz);
      return builder.created(new OClassEntity(oClass, io));
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }
}
