/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.providers;

import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.network.protocol.http.utils.Utils;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by Enrico Risa on 18/06/15.
 */
@OServerAuthenticated
@Provider
public class OServerSecurityRequestFilter implements ContainerRequestFilter {

  @Context
  ResourceInfo info;

  @Inject
  OServer      server;

  @Override
  public void filter(ContainerRequestContext containerRequestContext) throws IOException {

    MultivaluedMap<String, String> headers = containerRequestContext.getHeaders();

    OServerAuthenticated annotation = info.getResourceMethod().getAnnotation(OServerAuthenticated.class);

    String[] auth = Utils.decodeCredential(headers.getFirst("Authorization"));

    if (auth == null || !server.authenticate(auth[0], auth[1], annotation.resource())) {
      throw new WebApplicationException(Response.Status.UNAUTHORIZED);

    }
  }
}