package com.orientechnologies.orient.server.network.protocol.http.api.v1.services;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchemaProxy;

import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.*;

@Singleton
public class OClassService {

  private static final String ALIAS        = "alias";
  private static final String ABSTRACT     = "abstract";
  private static final String STRICTMODE   = "strictMode";
  private static final String SUPERCLASSES = "superClasses";
  private static final String NAME         = "name";
  private static final String TYPE         = "type";
  private static final String FIELDS       = "fields";

  public OClass singleClass(ODatabaseDocumentTx db, String name) {

    OClass aClass = db.getMetadata().getSchema().getClass(name);
    if (aClass == null) {
      String msg = String.format("Class %s not found.", name);
      throw new WebApplicationException(msg, Response.Status.NOT_FOUND);
    }
    return aClass;
  }

  public OClass alterClass(ODatabaseDocumentTx db, String className, Map<String, Object> clazz) {

    OClass aClass = singleClass(db, className);

    String name = (String) clazz.get(NAME);
    aClass.setName(name);
    setClassProperties(db.getMetadata().getSchema(), aClass, clazz);

    return aClass;
  }

  public OClass createClass(ODatabaseDocumentTx db, Map<String, Object> clazz) {

    checkInputParameter(clazz);

    try {
      OSchemaProxy schema = db.getMetadata().getSchema();
      String name = (String) clazz.get(NAME);
      OClass aClass = schema.createClass(name);
      setClassProperties(schema, aClass, clazz);
      return aClass;
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }

  }

  private void setClassProperties(OSchemaProxy schema, OClass aClass, Map<String, Object> clazz) {
    Boolean isAbstract = (Boolean) clazz.get(ABSTRACT);
    Boolean strictMode = (Boolean) clazz.get(STRICTMODE);
    String alias = (String) clazz.get(ALIAS);
    List<String> superClasses = (List<String>) clazz.get(SUPERCLASSES);
    if (isAbstract != null)
      aClass.setAbstract(isAbstract);
    if (alias != null)
      aClass.setShortName(alias);
    if (strictMode != null)
      aClass.setStrictMode(strictMode);

    if (superClasses != null) {
      List<OClass> classes = new ArrayList<OClass>();
      for (String superClass : superClasses) {
        OClass aClass1 = schema.getClass(superClass);
        if (aClass1 != null)
          classes.add(aClass1);
      }
      aClass.setSuperClasses(classes);
    }
  }

  private void checkInputParameter(Map<String, Object> clazz) {

    if (clazz.get(NAME) == null) {
      String msg = String.format("Name is required when creating class");
      throw new WebApplicationException(msg, Response.Status.BAD_REQUEST);
    }
  }

  public void dropClass(ODatabaseDocumentTx db, String name) {

    try {
      db.getMetadata().getSchema().dropClass(name);
    } catch (Exception e) {
      throw new WebApplicationException(e.getMessage(), e, Response.Status.BAD_REQUEST);
    }
  }

  public OIndex createIndex(ODatabaseDocumentTx db, String className, Map<String, Object> params) {
    checkInputIndexParams(params);
    OClass oClass = singleClass(db, className);
    String name = (String) params.get(NAME);
    String type = (String) params.get(TYPE);
    Collection<String> fields = (Collection<String>) params.get(FIELDS);

    return oClass.createIndex(name, type, fields.toArray(new String[fields.size()]));
  }

  public void dropIndex(ODatabaseDocumentTx db, String className, String indexName) {

    OIndex<?> index = db.getMetadata().getIndexManager().getIndex(indexName);
    index.delete();
  }

  public void rebuildIndex(ODatabaseDocumentTx db, String className, String indexName) {

    OIndex<?> index = db.getMetadata().getIndexManager().getIndex(indexName);
    index.rebuild();
  }

  private void checkInputIndexParams(Map<String, Object> params) {

    if (params.get(NAME) == null) {
      String msg = String.format("Name is required when creating an index");
      throw new WebApplicationException(msg, Response.Status.BAD_REQUEST);
    }
    if (params.get(TYPE) == null) {
      String msg = String.format("Type is required when creating an index");
      throw new WebApplicationException(msg, Response.Status.BAD_REQUEST);
    }
  }

  public Set<OIndex<?>> indexes(ODatabaseDocumentTx db, String className) {
    return singleClass(db, className).getIndexes();
  }
}
