/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.io.writer;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public class OIOWriterJson implements OIOWriter {

  private final ByteArrayOutputStream stream;
  private JsonGenerator               generator;

  public OIOWriterJson(JsonFactory factory, ByteArrayOutputStream stream) throws IOException {

    this.generator = factory.createGenerator(stream);
    this.stream = stream;
  }

  @Override
  public void startObject() throws Exception {
    generator.writeStartObject();
  }

  @Override
  public void endObject() throws Exception {

    generator.writeEndObject();
  }

  @Override
  public void writeAttribute(String name, Object value) throws Exception {

    if (value == null) {
      generator.writeNullField(name);
    } else {
      writeDerivedType(name, value);
    }

  }

  @Override
  public void writeValue(Object value) throws Exception {

    if (value == null) {
      generator.writeNull();
    } else {
      writeDerivedType(null, value);
    }

  }

  @Override
  public void startCollection(String name) throws Exception {

    generator.writeFieldName(name);
    generator.writeStartArray();
  }

  @Override
  public void startCollection() throws Exception {
    generator.writeStartArray();
  }

  @Override
  public void endCollection() throws Exception {

    generator.writeEndArray();
  }

  @Override
  public String end() throws Exception {
    generator.close();
    return stream.toString("UTF8");
  }

  public OutputStream stream() {
    return stream;
  }

  // DELEGATE

  public void writeBoolean(boolean state) throws IOException {
    generator.writeBoolean(state);
  }

  public void writeNumber(String encodedValue) throws IOException {
    generator.writeNumber(encodedValue);
  }

  public void writeNumber(BigDecimal v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumber(float v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumber(double v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumber(BigInteger v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumber(long v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumber(int v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumber(short v) throws IOException {
    generator.writeNumber(v);
  }

  public void writeNumberField(String fieldName, int value) throws IOException {
    generator.writeNumberField(fieldName, value);
  }

  public void writeNumberField(String fieldName, long value) throws IOException {
    generator.writeNumberField(fieldName, value);
  }

  public void writeNumberField(String fieldName, double value) throws IOException {
    generator.writeNumberField(fieldName, value);
  }

  public void writeNumberField(String fieldName, float value) throws IOException {
    generator.writeNumberField(fieldName, value);
  }

  public void writeNumberField(String fieldName, BigDecimal value) throws IOException {
    generator.writeNumberField(fieldName, value);
  }

  protected void writeBooleanField(String name, Boolean value) throws Exception {
    generator.writeBooleanField(name, value.booleanValue());
  }

  public void writeStringField(String fieldName, String value) throws IOException {
    generator.writeStringField(fieldName, value);
  }

  public void writeString(String text) throws IOException {
    generator.writeString(text);
  }

  protected void writeStringAttreibute(String name, String value) throws Exception {
    generator.writeStringField(name, value);
  }

  // TODO don't like this. find a better way
  protected void writeDerivedType(String name, Object value) throws Exception {

    if (value instanceof String) {
      if (name != null) {
        writeStringField(name, value.toString());
      } else {
        writeString(value.toString());
      }
      return;
    }
    if (value instanceof Boolean) {
      if (name != null) {
        writeBooleanField(name, (Boolean) value);
      } else {
        writeBoolean((Boolean) value);
      }
    }
  }
}
