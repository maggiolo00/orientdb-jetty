/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *  
 */

package com.orientechnologies.orient.server.network.protocol.http.io.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orientechnologies.orient.core.record.impl.ODocument;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Enrico Risa on 21/06/15.
 */
public class OIOReaderJson implements OIOReader {

  ObjectMapper mapper = new ObjectMapper();

  @Override
  public Map<String, Object> parseMap(String input) throws IOException {
    return mapper.readValue(input, Map.class);
  }

  @Override
  public ODocument parseDocument(String input) {
    return new ODocument().fromJSON(input);
  }
}
