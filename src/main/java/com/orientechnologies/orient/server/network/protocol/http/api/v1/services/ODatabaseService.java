package com.orientechnologies.orient.server.network.protocol.http.api.v1.services;

import com.orientechnologies.orient.core.Orient;
import com.orientechnologies.orient.core.db.ODatabase;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.engine.local.OEngineLocalPaginated;
import com.orientechnologies.orient.core.engine.memory.OEngineMemory;
import com.orientechnologies.orient.server.OServer;
import groovy.lang.Singleton;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Enrico Risa on 18/06/15.
 */
@Singleton
public class ODatabaseService {

  private static final String TYPE    = "type";
  private static final String STORAGE = "storage";
  private static final String NAME    = "name";

  @Inject
  OServer                     server;

  public Map<String, String> listDatabases() {
    return server.getAvailableStorageNames();
  }

  public ODatabaseDocumentTx createDatabase(Map<String, Object> command) {

    checkConfig(command);

    String name = (String) command.get(NAME);
    String storage = (String) command.get(STORAGE);

    String url = getStoragePath(name, storage);
    String type = (String) command.get(TYPE);

    ODatabaseDocumentTx db = createDb(type, url);

    return db;
  }

  private void checkConfig(Map<String, Object> command) {

    String name = (String) command.get(NAME);
    String storage = (String) command.get(STORAGE);

    if (name == null || storage == null) {
      // TODO

      String msg = String.format("Name or storage required.");
      throw new WebApplicationException(msg, Response.Status.BAD_REQUEST);
    }
    String type = (String) command.get(TYPE);

    if (type == null) {
      command.put(TYPE, "document");
    }
  }

  private ODatabaseDocumentTx createDb(String type, String url) {
    ODatabaseDocumentTx database = Orient.instance().getDatabaseFactory().createDatabase(type, url);

    if (database.exists()) {
      String msg = String.format("Database %s already exists.", database.getURL());
      throw new WebApplicationException(msg, Response.Status.CONFLICT);
    } else {
      database.create();
    }
    return database;
  }

  protected String getStoragePath(final String databaseName, final String iStorageMode) {
    if (iStorageMode.equals(OEngineLocalPaginated.NAME))
      return iStorageMode + ":" + server.getDatabaseDirectory() + databaseName;
    else if (iStorageMode.equals(OEngineMemory.NAME))
      return iStorageMode + ":" + databaseName;

    return null;
  }

  public Map<String, Object> dropDatabase(String database, String username, String password) {

    try {
      final ODatabase<?> db = server.openDatabase("document", database, username, password);
      db.drop();

    } finally {

    }
    return new HashMap<String, Object>();
  }

  public void dropDatabase(ODatabaseDocument databaseDocument) {
    databaseDocument.drop();
  }
}
