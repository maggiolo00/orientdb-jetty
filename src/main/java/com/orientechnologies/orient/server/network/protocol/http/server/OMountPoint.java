package com.orientechnologies.orient.server.network.protocol.http.server;

/**
 * Created by Enrico Risa on 17/06/15.
 */
public interface OMountPoint {

  String mountPoint();

  String packages();
}
