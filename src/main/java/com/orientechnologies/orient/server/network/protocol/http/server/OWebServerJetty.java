/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.server;

import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.network.protocol.http.binders.DefaultBinder;
import com.orientechnologies.orient.server.network.protocol.http.providers.OProviderManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * Created by Enrico Risa on 17/06/15.
 */
public class OWebServerJetty implements OWebServer {

  Server           server;
  OServer          oServer;

  OWebServerConfig config          = new OWebServerConfig();

  OProviderManager providerManager = new OProviderManager();

  @Override
  public OWebServer setPort(int port) {
    config.port = port;
    return this;
  }

  @Override
  public void mount(OMountPoint mountPoint) {

    ResourceConfig config = new ResourceConfig();
    config.register(new DefaultBinder(oServer));
    config.packages(mountPoint.packages(), providerManager.packages());
    ServletContainer container = new ServletContainer(config);
    ServletHolder sh = new ServletHolder(container);
    ServletContextHandler context = new ServletContextHandler(server, mountPoint.mountPoint(), ServletContextHandler.SESSIONS);
    context.addServlet(sh, "/*");

  }

  @Override
  public void umount(OMountPoint mountPoint) {

  }

  @Override
  public OWebServer init() {
    server = new Server(config.port);
    initContext();
    return this;
  }

  @Override
  public OWebServer setServer(OServer server) {
    oServer = server;
    return this;
  }

  private void initContext() {

  }

  @Override
  public void start() {

    if (server == null) {
      // throws exception
      server = new Server(config.port);
    }
    try {
      server.start();
//      server.dumpStdErr();
      server.join();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stop() {

  }

}
