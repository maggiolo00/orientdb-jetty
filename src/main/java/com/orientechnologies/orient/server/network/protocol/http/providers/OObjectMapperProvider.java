/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.providers;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Created by Enrico Risa on 18/06/15.
 */

@Provider
public class OObjectMapperProvider implements ContextResolver<ObjectMapper> {

  final ObjectMapper defaultObjectMapper;
  final ObjectMapper combinedObjectMapper;

  public OObjectMapperProvider() {
    defaultObjectMapper = createDefaultMapper();
    combinedObjectMapper = createCombinedObjectMapper();
  }

  @Override
  public ObjectMapper getContext(final Class<?> type) {
    return defaultObjectMapper;
  }

  private static ObjectMapper createCombinedObjectMapper() {
    return new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, true)
        .configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
        .setAnnotationIntrospector(createJaxbJacksonAnnotationIntrospector());
  }

  private static ObjectMapper createDefaultMapper() {
    final ObjectMapper result = new ObjectMapper();
    result.enable(SerializationFeature.INDENT_OUTPUT);

    return result;
  }

  private static AnnotationIntrospector createJaxbJacksonAnnotationIntrospector() {

    final AnnotationIntrospector jaxbIntrospector = new JaxbAnnotationIntrospector(TypeFactory.defaultInstance());
    final AnnotationIntrospector jacksonIntrospector = new JacksonAnnotationIntrospector();

    return AnnotationIntrospector.pair(jacksonIntrospector, jaxbIntrospector);
  }
}
