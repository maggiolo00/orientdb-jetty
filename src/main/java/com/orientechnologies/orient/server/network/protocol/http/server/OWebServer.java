package com.orientechnologies.orient.server.network.protocol.http.server;

import com.orientechnologies.orient.server.OServer;

/**
 * Created by Enrico Risa on 17/06/15.
 */
public interface OWebServer {

  public OWebServer setPort(int port);

  public OWebServer init();

  public OWebServer setServer(OServer server);

  public void mount(OMountPoint mountPoint);

  public void umount(OMountPoint mountPoint);

  public void start();

  public void stop();
}
