package com.orientechnologies.orient.server.network.protocol.http;

import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.config.OServerParameterConfiguration;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.OApiV1MountPoint;
import com.orientechnologies.orient.server.network.protocol.http.server.OWebServer;
import com.orientechnologies.orient.server.network.protocol.http.server.OWebServerFactory;
import com.orientechnologies.orient.server.plugin.OServerPluginAbstract;

/**
 * Created by Enrico Risa on 28/04/15.
 */
public class OWebServerPlugin extends OServerPluginAbstract {

  OWebServer server;

  OServer    oServer;

  @Override
  public String getName() {
    return "Web Server Plugin";
  }

  @Override
  public void startup() {
    super.startup();
    new Thread() {
      @Override
      public void run() {
        server = OWebServerFactory.INSTANCE.createWebServer();
        server.setServer(oServer).init();
        loadInternalMountPoint();
        server.start();
      }
    }.start();
  }

  @Override
  public void shutdown() {
    super.shutdown();
    server.stop();
  }

  @Override
  public void config(OServer oServer, OServerParameterConfiguration[] iParams) {
    super.config(oServer, iParams);
    this.oServer = oServer;
  }

  public void loadInternalMountPoint() {
    server.mount(new OApiV1MountPoint());
  }
}
