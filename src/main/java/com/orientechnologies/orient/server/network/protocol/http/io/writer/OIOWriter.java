/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.io.writer;

/**
 * Created by Enrico Risa on 20/06/15.
 */
public interface OIOWriter {

  public void startObject() throws Exception;

  public void endObject() throws Exception;

  public void writeAttribute(String name, Object value) throws Exception;

  public void writeValue(Object value) throws Exception;

  public void startCollection(String name) throws Exception;

  public void startCollection() throws Exception;

  public void endCollection() throws Exception;

  public String end() throws Exception;
}
