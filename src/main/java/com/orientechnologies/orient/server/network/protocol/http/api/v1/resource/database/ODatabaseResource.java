/*
 *
 *  *
 *  *  *  Copyright 2015 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *  *
 *  *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  *  you may not use this file except in compliance with the License.
 *  *  *  You may obtain a copy of the License at
 *  *  *
 *  *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *  *
 *  *  *  Unless required by applicable law or agreed to in writing, software
 *  *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  *  See the License for the specific language governing permissions and
 *  *  *  limitations under the License.
 *  *  *
 *  *  * For more information: http://www.orientechnologies.com
 *  *
 *
 */

package com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.OApiV1MountPoint;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database.document.ODocumentResource;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.resource.database.schema.OClassResource;
import com.orientechnologies.orient.server.network.protocol.http.api.v1.services.ODatabaseService;
import com.orientechnologies.orient.server.network.protocol.http.io.OAbstractIO;
import com.orientechnologies.orient.server.network.protocol.http.io.OResponseBuilder;
import com.orientechnologies.orient.server.network.protocol.http.io.mapping.ODatabaseEntity;
import com.orientechnologies.orient.server.network.protocol.http.providers.DB;
import com.orientechnologies.orient.server.network.protocol.http.providers.IO;
import com.orientechnologies.orient.server.network.protocol.http.providers.OServerAuthenticated;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Enrico Risa on 18/06/15.
 */
@Path(OApiV1MountPoint.DATABASES_ROOT)
public class ODatabaseResource {

  @Inject
  ODatabaseService databaseService;

  @Inject
  OResponseBuilder builder;

  /**
   * @api {post} /databases Create Database
   * @apiName CreateDatabase
   * @apiGroup Database
   *
   * @apiParam {String} name Database name
   * @apiParam {String} storage Storage type
   * @apiParam {String} type Database type
   *
   *
   * @apiSuccessExample Success-Response: HTTP/1.1 201 OK
   * {
   *    "name" :
   *    "dbName"
   * }
   * @apiError DatabaseNotCreated
   *
   * @apiErrorExample Error-Response: HTTP/1.1 404 Not Found
   *
   *                  { "error": "DatabaseNotCreated" }
   */

  @POST
  @OServerAuthenticated(resource = "database.create")
  @Produces(MediaType.APPLICATION_JSON)
  public Response createDatabase(String body, @IO OAbstractIO io) throws IOException {

    Map<String, Object> msg = io.reader().parseMap(body);

    ODatabaseDocumentTx db = databaseService.createDatabase(msg);

    return builder.created(new ODatabaseEntity(db, io));
  }

  /**
   * @api {get} /databases Get Database
   * @apiName GetDatabases
   * @apiGroup Database
   *
   *
   *
   * @apiSuccessExample Success-Response: HTTP/1.1 201 OK
   *
   *                    { "name" : "dbName" }
   *
   * @apiError DatabaseNotCreated
   *
   * @apiErrorExample Error-Response: HTTP/1.1 404 Not Found
   *
   *                  { "error": "DatabaseNotCreated" }
   */

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response listDatabases() {
    return Response.ok().entity(databaseService.listDatabases()).build();
  }

  /**
   * @api {delete} /databases/:name Drop Database
   * @apiName DropDatabase
   * @apiGroup Database
   *
   * @apiParam {String} name Database name
   *
   * @apiSuccessExample Success-Response: HTTP/1.1 201 OK
   *
   *                    { "name" : "dbName" }
   *
   * @apiError DatabaseNotCreated
   *
   * @apiErrorExample Error-Response: HTTP/1.1 404 Not Found
   *
   *                  { "error": "DatabaseNotCreated" }
   */

  @DELETE
  @OServerAuthenticated(resource = "database.drop")
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{database}")
  public Response dropDatabase(@PathParam("database") String database, @DB ODatabaseDocumentTx databaseDocument) {
    databaseService.dropDatabase(databaseDocument);
    return builder.ok();
  }

  @GET
  @Path("{database}")
  public Response singleDb(@PathParam("database") String database, @IO OAbstractIO io, @DB ODatabaseDocumentTx db) {
    return builder.ok(new ODatabaseEntity(db, io));
  }

  @Path("{database}/classes")
  public Class<OClassResource> classes() {
    return OClassResource.class;
  }

  @Path("{database}/documents")
  public Class<ODocumentResource> documents() {
    return ODocumentResource.class;
  }
}
